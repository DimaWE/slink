<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\commands;

use yii\console\Controller;
use yii\console\ExitCode;

use app\models\Weather;
use app\tools\WeatherParse;

/**
 * This command echoes the first argument that you have entered.
 *
 * This command is provided as an example for you to learn how to create console commands.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class WeatherController extends Controller
{
    public function actionGet($message = 'hello world')
    {
        $wp = new WeatherParse("https://yandex.ru/pogoda/moscow/details");
        $wpData = $wp->weatherParse();

        foreach ($wpData as $dayData) {

            $weatherModel = Weather::findOne([
                'date' => $dayData['date']
            ]);
            if(!$weatherModel) {
                $weatherModel = new Weather();
                $weatherModel->date = $dayData['date'];
            }

            $weatherModel->morning = json_encode($dayData['morning']);
            $weatherModel->day = json_encode($dayData['day']);
            $weatherModel->evening = json_encode($dayData['evening']);
            $weatherModel->night = json_encode($dayData['night']);

            $weatherModel->save();
        }

        return ExitCode::OK;
    }
}
