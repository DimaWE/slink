<?php

use yii\db\Migration;

/**
 * Handles the creation of table `links`.
 */
class m190207_130803_create_links_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute("
            CREATE TABLE `links` (
                `code` varchar(7) COLLATE utf8mb4_unicode_ci NOT NULL,
                `url` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

            ALTER TABLE `links`
            ADD PRIMARY KEY (`code`),
            ADD KEY `url` (`url`);
            COMMIT;                      
        ");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('links');
    }
}
