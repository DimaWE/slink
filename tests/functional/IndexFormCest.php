<?php

class ContactFormCest 
{
    public function _before(\FunctionalTester $I)
    {
        $I->amOnPage(['/site/index-test']);
    }

    public function openIndexPage(\FunctionalTester $I)
    {
        $I->see('Введите ссылку');
        $I->see('Go!');
        
    }

    public function goLinkPage(\FunctionalTester $I)
    {
        for ($i=0; $i < 10000; $i++) { 
            $url = "https://site{$i}.com/";
            $I->fillField('#url', $url);
            $I->click('#generate-btn');
        }   
    }

}
