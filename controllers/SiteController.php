<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Link;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex(): string
    {     
        return $this->render('index');     
    }

    public function actionGoLink(string $linkCode)
    {
        $link = Link::findOne($linkCode);
        if($link) 
            return $this->redirect($link->url);

        throw new \yii\web\NotFoundHttpException("Link not found");
        
    }

    public function actionGenerateLink(): string 
    {
        $url = Yii::$app->request->post('url');
        $link = Link::findOne(['url' => $url]);
        if($link) {
            return json_encode([
                'success' => true,
                'link' => \yii\helpers\BaseUrl::to($link->code, true)
                ]);
        }
        else {
            $link = new Link;
            $link->url = $url;
            $link->code = $link->getCode();
            if(filter_var(idn_to_ascii($url), FILTER_VALIDATE_URL) && $link->save())
                return json_encode([
                    'success' => true,
                    'link' => \yii\helpers\BaseUrl::to($link->code, true)
                    ]);
            else {
                return json_encode([
                    'success' => false,
                    'message' => 'Введите корректный URL'
                    ]); 
            }
        }
    }

    public function actionIndexTest(): string
    {    
        $this->actionGenerateLink();
        return $this->render('index-test');     
    }
}
