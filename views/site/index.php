<?php

/* @var $this yii\web\View */

$this->title = 'Генератор коротких url';
?>


<div class="container">
    <div class="page-header">
        <h1>Введите ссылку</h1>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <form>
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="URL" id="url" name="url">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button" id="generate-btn">Go!</button>
                    </span>
                </div>
            </form>
        </div>
    </div>

    <div class="row">
        <div class="link col-lg-6">
            <a href="/" target="_blank">fff</a>
        </div>
    </div>

    <div class="row">
        <div class="error-msg col-lg-6 alert alert-danger">
        </div>
    </div>
</div>


