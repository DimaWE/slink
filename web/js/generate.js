$('#generate-btn').on('click', function() {
    let url = $('#url');
    $.ajax({
        type: "POST",
        url: '/site/generate-link',
        data: {'url' : url.val()},
        success: function(data) {
            data = JSON.parse(data);
            if(data.success) {
                $('.error-msg').hide();
                url.val('');
                $('.link > a').attr('href', data.link).text(data.link);
                $('.link').show();
            } else {
                $('.link').hide();
                $('.error-msg').html(data.message).show();
            }
        }
      });    
});