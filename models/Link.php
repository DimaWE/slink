<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "links".
 *
 * @property string $code
 * @property string $url
 */
class Link extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'links';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['code', 'url'], 'required'],
            [['code'], 'string', 'max' => 7],
            [['url'], 'string', 'max' => 200],
            [['code'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'code' => 'Code',
            'url' => 'Url',
        ];
    }

    public function getCode(): string
    {
        while(1) {
            $code = \Yii::$app->security->generateRandomString(7);
            $link = self::findOne($code);
            if(!$link)
                return $code;
        }
    }
}
